<?php
/**
 * Copyright (c) 2019 Florian Levis.
 * Distributed under the MIT License (http://opensource.org/licenses/MIT)
 */
declare(strict_types=1);

namespace Gounlaf\Webmozart\Assert;

use DateTime;

/**
 * @method static void nullOrDateFormat(string $value, string $dateFormat, string $message = '')
 * @method static void allDateFormat($values, string $dateFormat, string $message = '')
 */
trait Date
{
    /**
     * @param string $value String representing the time.
     * @param string $dateFormat Format accepted by date().
     * @param string $message
     *
     * @return void
     */
    public static function dateFormat(string $value, string $dateFormat, string $message = '')
    {
        $d = DateTime::createFromFormat($dateFormat, $value);

        if (false === $d || $d->format($dateFormat) !== $value) {
            static::reportInvalidArgument(sprintf(
                $message ?: 'Expected a date with format %s',
                $dateFormat
            ));
        }
    }
}
