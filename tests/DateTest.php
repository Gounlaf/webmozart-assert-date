<?php

declare(strict_types=1);

namespace Gounlaf\Webmozart\Assert\Test;

use Gounlaf\Webmozart\Assert\Test\stub\AssertTest;
use PHPUnit\Framework\TestCase;

final class DateTest extends TestCase
{
    public function testDateFormatFromString()
    {
        $this->expectNotToPerformAssertions();
        AssertTest::dateFormat('2019-03-22 22:00:00', 'Y-m-d H:i:s');
    }

    public function testAllDateFormatFromString()
    {
        $this->expectNotToPerformAssertions();
        AssertTest::allDateFormat(['2019-03-22 22:00:00'], 'Y-m-d H:i:s');
    }

    public function testDateFormatException()
    {
        $this->expectException(\InvalidArgumentException::class);
        AssertTest::dateFormat('2019-03-22 22-00-00', 'Y-m-d H:i:s');
    }

    public function testAllDateFormatException()
    {
        $this->expectException(\InvalidArgumentException::class);
        AssertTest::allDateFormat(['2019-03-22 22-00-00'], 'Y-m-d H:i:s');
    }
}
