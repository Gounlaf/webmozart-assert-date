<?php

declare(strict_types=1);

namespace Gounlaf\Webmozart\Assert\Test\stub;

use Gounlaf\Webmozart\Assert\Date;
use Webmozart\Assert\Assert;

final class AssertTest extends Assert
{
    use Date;
}
