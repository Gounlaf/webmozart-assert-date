# Date extension for webmozart/assert

A trait to be used as extension of Assert class from webmozart/assert 

## License

This project is licensed under the MIT license. Please see the `LICENSE` file
for more information.
